# Instructions
To run the project, clone the repository, install the dependencies and start running. This can be done using the commands:

    npm install
    npm run runHeadless --spec "cypress/e2e/AQA-SR/APITest.cy.js,cypress/e2e/AQA-SR/webAutomation.cy.js" or npx cypress run
	
	If you want start the execution and see how the tests are running in the browser, it is necessary to execute the command npx cypress open.

# Automated API Test
## Introduction
The Automated API Test project aims to perform the following tasks:

1. Send a GET request to the API endpoint https://api.publicapis.org/entries.
2. Verify that the response status code is 200 (OK).
3. Compare, count, and verify the objects with the "Category: Authentication & Authorization."


# Web Automation
## Introduction
The Cypress Test Suite consists of automated tests that perform the following tasks:

1. Validates a successful login to the web application https://www.saucedemo.com/.
2. Verifies the title on the home page.
3. Logs in and checks if the items are ordered correctly in descending order of Name (Z -> A).

The commands.js class carries out the commands to actually execute web automation, while the locators class has the elements to locate the fields on the page.

------------

# GitLab CI/CD Configuration:
This repository includes the GitLab CI/CD configuration, which automates the testing of project.

1. Clone this repository.
2. Configure GitLab Runner to enable automated testing.
3. The pipeline will run automatically upon code changes or can be triggered manually.
